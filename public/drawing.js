//helper functions for drawing stuff on html5 canvases

/*
Provides functions:
	circle(x, y, r, colour*, outline*, outline_thickness*)
	hex(x, y, r, colour*, outline*, outline_thickness*)
	text(x, y, text, colour*, font_size*, corner*)
	clearCanvas()
	distance(x0, y0, x1, y1)
Arguments marked with an asterisk here may be undefined.
*/

const DRAWING_CANVAS = document.getElementById("thegame");
const DRAWING_HEIGHT = DRAWING_CANVAS.height;
const DRAWING_WIDTH = DRAWING_CANVAS.width;
const DRAWING_CENTRE = [DRAWING_WIDTH/2, DRAWING_HEIGHT/2];
const DRAWING_CTX = DRAWING_CANVAS.getContext("2d");

const tau = Math.PI * 2;
const cos30 = Math.cos(Math.PI / 6);
const cos60 = Math.cos(Math.PI / 3);
const DRAWING_HEX_X = [1, cos60, -cos60, -1, -cos60, cos60];
const DRAWING_HEX_Y = [0, -cos30, -cos30, 0, cos30, cos30];



//colourize: to be called after most drawing operations
function colourize(colour, outline, stroke_width)
{
	if (colour) {
		DRAWING_CTX.fillStyle = colour;
		DRAWING_CTX.fill();
	}
	if (outline) {
		DRAWING_CTX.strokeStyle = outline;
		DRAWING_CTX.stroke();
	}
	DRAWING_CTX.beginPath();
}

function line(x0, y0, x1, y1, colour, thickness)
{
	DRAWING_CTX.moveTo(x0, y0);
	DRAWING_CTX.lineTo(x1, y1);
	colourize(undefined, colour, thickness);
}

//circle: draws a circle around (x, y) with radius r
function circle(x, y, r, colour, outline, outline_thickness)
{
	DRAWING_CTX.arc(x, y, r, 0, tau);
	colourize(colour, outline, outline_thickness);
}

//hex: draws a flat-topped hexagon around (x, y) with distance r to corners
function hex(x, y, r, colour, outline, outline_thickness)
{
	DRAWING_CTX.moveTo(x + DRAWING_HEX_X[5] * r, y + DRAWING_HEX_Y[5] * r);
	for (let i = 0; i < 6; ++i) {
		DRAWING_CTX.lineTo(x + DRAWING_HEX_X[i] * r, y + DRAWING_HEX_Y[i] * r);
	}
	DRAWING_CTX.closePath();
	colourize(colour, outline, outline_thickness);
}

//text: draws string text at (x, y) with given font size
//		corner: if true, (x, y) is the top left corner instead of the centre
function text(x, y, text, colour, font_size, corner)
{
	DRAWING_CTX.fillStyle = colour;
	if (corner) {
		DRAWING_CTX.textAlign = "left";
		DRAWING_CTX.textBaseline = "top";
	} else {
		DRAWING_CTX.textAlign = "center";
		DRAWING_CTX.textBaseline = "middle";
	}
	DRAWING_CTX.font = font_size+"px sans-serif";
	DRAWING_CTX.fillText(text, x, y);
}

function clearCanvas()
{
	DRAWING_CTX.clearRect(0, 0, DRAWING_WIDTH, DRAWING_HEIGHT);
}

//distance between two points (x0, y0), (x1, y1)
function distance(x0, y0, x1, y1)
{
	let x_diff = x0 - x1;  let y_diff = y0 - y1;
	return Math.sqrt(x_diff*x_diff + y_diff*y_diff);
}

