//top, topleft, botleft, bottom, botright, topright
const DELTA_LIST = [hexCoords(0,1,-1), hexCoords(-1,1,0), hexCoords(-1,0,1), hexCoords(0,-1,1), hexCoords(1,-1,0), hexCoords(1,0,-1)];


function renderingCoords(hexCoords)
{
	let x = hexCoords.x * 1.5;
	let y = cos30 * (hexCoords.z - hexCoords.y);
	return [x, y];
}

function hexCoordsToString(hexCoords)
{
	return hexCoords.x + "," + hexCoords.y + "," + hexCoords.z;
}

function hexCoords(x, y, z)
{
	return {x:x, y:y, z:z};
}

function addHexCoords(hc1, hc2)
{
	return hexCoords(hc1.x + hc2.x, hc1.y + hc2.y, hc1.z + hc2.z);
}

function sideToDelta(side)
{
	return DELTA_LIST[side % 6];
}

function edgeCoords(side)
{
	let angle = side * Math.PI / 3;
	return [-Math.sin(angle) * cos30, -Math.cos(angle) * cos30];
}

//returns an arbitrary node from the neighbouring cell on the given side
//with a connection to the given cell,
//or undefined if no such node exists
function findNodeOnSide(cell, side)
{
	let delta = sideToDelta(side);
	let other_cell = hexMap[addHexCoords(cell.getPos(), delta)];
	if (!other_cell) {
		return undefined;
	}
	let circuit = other_cell.getCircuit();
	if (!circuit) {
		return undefined;
	}
	let opposite_side = (side + 3) % 6;
	let nodes = circuit.getNodes();
	for (var i = nodes.length - 1; i >= 0; i--) {
		let node = nodes[i];
		let connections = node.getConnections;
		if (connections.indexOf(opposite_side) > -1) {
			return node;
		}
	}
	return undefined;
}

function mergeNetworks(network1, network2)
{
	let nodes = network2.getNodes();
	for (var i =  nodes.length - 1; i >= 0; i--) {
		 let node = nodes[i];
		 node.setNetwork(network1);
	}
	network1.mergeNetwork(network2);
}

function addNodeToNetwork(network, node)
{
	let old_network = node.getNetwork();
	if (!old_network) {
		node.setNetwork(network);
		network.addNode(node);
	}
	else if (old_network !== network) {
		mergeNetworks(old_network, network);
	}
}

function addNode(node)
{
	let connections = node.getConnections();
	for (var i = connections.length - 1; i >= 0; i--) {
		let side = connections[i];
		let network = findNodeOnSide(node.getCell(), side);
		if (network) {
			addNodeToNetwork(network, node);
		}
	}
	if (!node.getNetwork()) {
		node.setNetwork(new Network([node]));
	}
}

function recalculateNetwork(network)
{
	let network_nodes = network.getNodes();
	let unchecked_nodes = [];
	while (network_nodes.length > 0) {
		let checked_nodes = [];
		unchecked_nodes.clear();
		unchecked_nodes.push(network_nodes[0]);
		while (unchecked_nodes.length > 0) {
			let node = unchecked_nodes.pop();
			let connections = node.getAllConnections();
			for (var i = connections.length - 1; i >= 0; i--) {
				let other_node = connections[i];
				if (checked_nodes.indexOf(other_node) > -1) {
					unchecked_nodes.push(other_node);
				}
			}
			checked_nodes.push(node);
		}
		network_nodes = network_nodes.filter(function(i){return checked_nodes.indexOf(i) > -1});
	}
	delete networkList[indexOf(network)];
}

function removeCircuit(cell, circuit)
{
	networks_to_recalc = [];
	let nodes = circuit.getNodes();
	for (var i = nodes.length - 1; i >= 0; i--) {
		network = nodes[i].getNetwork();
		if (network) {
			if (networks_to_recalc.indexOf(network) < 0) {
				networks_to_recalc.push(network);
			}
			delete network.getNodes().indexOf(nodes[i]);
		}
		delete nodes[i];
	}
	cell.setCircuit(null);
}


function createCircuit(componentList, cell, prefab)
{
	if (prefab == "source") {
		let node1 = new Node(cell, [0,1,2,3,4,5], 0);
		let logic = function(comp) {comp.setPort("a", 1);}
		let io = {"a": node1};
		let component1 = new Component(logic, io, cell);
		let circuit = new Circuit(0, [component1], [node1]);
		cell.setCircuit(circuit);
		componentList.push(component1);
		circuit.init();
	}
}


class Circuit
{
	constructor(rotation, components, nodes)
	{
		this.rot_ = rotation;
		this.components_ = components;
		this.nodes_ = nodes;
	}

	init()
	{
		for (var i = this.nodes_.length - 1; i >= 0; i--) {
			this.nodes_[i].init();
		}
	}

	getRot()
	{
		return this.rot_;
	}

	getComponents()
	{
		return this.components_;
	}

	getNodes()
	{
		return this.nodes_;
	}
}


class Cell
{
	constructor(pos)
	{
		this.pos_ = pos;
		this.circuit_ = null;
	}

	getPos()
	{
		return this.pos_;
	}

	setCircuit(circuit)
	{
		this.circuit_ = circuit;
	}
	getCircuit()
	{
		return this.circuit_;
	}

}

class Node
{
	constructor(cell, connections, rotation)
	{
		//list of connected component inputs - not needed, component's responsibility
		//this.outputs_ = [];
		//list of cell sides this node connects to
		this.connections_ = connections;
		this.local_connections_ = [];
		//every node must belong to a network, but that is yet to be calculated
		this.network_ = undefined;
		this.cell_ = cell;
		this.rot_ = rotation;
		this.input_ = undefined;
	}

	init()
	{
		this.calculateLocalConnections();
		let components = this.cell_.getCircuit().getComponents();
		for (var i = components.length - 1; i >= 0; i--) {
			let comp = components[i];
			let input = comp.getOutput(this);
			if (input) {
				this.input_ = input;
				break;
			}
		}
		addNode(this);
	}

	rotate(delta)
	{
		this.rot_ += delta;
		for (let i = connections_.length - 1; i >= 0; i--) {
			connections_[i] = (connections_[i] + delta) % 6;
		}
	}

	calculateLocalConnections()
	{
		let nodes = this.cell_.getCircuit().getNodes();
		for (var i = nodes.length - 1; i >= 0; i--) {
			let node = nodes[i];
			if (this !== node && hasCommonElement(this.connections_, node.getConnections())) {
				this.local_connections_.push(node);
			}
		}
	}

	getConnections()
	{
		return this.connections_;
	}

	getAllConnections()
	{
		return this.connections_.concat(this.local_connections_);
	}

	setNetwork(network)
	{
		this.network_ = network;
	}
	getNetwork()
	{
		return this.network_;
	}

	getCell()
	{
		return this.cell_;
	}

	getState()
	{
		if (this.input_) {
			return this.input_();
		}
		return undefined;
	}

	getInput()
	{
		return this.input_;
	}
}

class Network
{
	constructor(nodes)
	{
		this.nodes_ = nodes;
		// a network's state is undefined if its input count is not 1
		// (or its only input's state is undefined)
		// an input is a component's output NODE
		this.inputs_ = [];
		this.state_ = undefined;
		networkList.push(this);
		if (nodes && nodes.length > 0) {
			this.autoAddInputs();
		}
	}

	getNodes()
	{
		return this.nodes_;
	}

	addNode(node)
	{
		this.nodes_.push(node);
		this.addInput(node);
	}

	addInput(node)
	{
		if (node.getInput()) {
			this.inputs_.push(node);
		}
	}

	autoAddInputs()
	{
		for (var i = this.nodes_.length - 1; i >= 0; i--) {
			this.addInput(this.nodes_[i]);
		}
	}

	getInputs()
	{
		return this.inputs_;
	}

	mergeNetwork(other_network)
	{
		this.nodes_ = this.nodes_.concat(other_network.getNodes());
		this.inputs_ = this.inputs_.concat(other_network.getInputs());
	}

	updateState()
	{
		if (this.inputs_.length != 1) {
			this.state_ = undefined;
		} else {
			this.state_ = this.inputs_[0].getState();
		}
	}
	getState()
	{
		return this.state_;
	}
}

class Component
{
	constructor(logic, io, cell)
	{
		// function that interacts with its ports
		this.logic_ = logic;
		this.cell_ = cell;
		// {portName: node}
		// these nodes are on the same cell as the component so no worry about rotation
		this.io_ = io;
		// {outPortName: state}
		this.state_ = {};
	}

	clock()
	{
		this.logic_(this);
	}

	setPort(port, state)
	{
		this.state_[port] = state;
	}

	readPort(port)
	{
		return this.state_[port];
	}

	// if this component outputs to the given node,
	// this returns a function to read the outputting port
	// otherwise, returns null
	getOutput(node)
	{
		let keys = Object.keys(this.io_);
		for (var i = keys.length - 1; i >= 0; i--) {
			let port = keys[i];
			let portNode = this.io_[port];
			if (portNode === node) {
				var comp = this;
				return function() {
					return comp.readPort(port);
				}
			}
		}
		return null;
	}
}
