
var screenInfo = {x: -300, y: -300, hexScale: 50, dirty: true};
var hexMap;
var componentList;
var networkList;

const stateColors = {"0": "black", "1": "blue", "-1": "purple", "undefined": "red"};


function drawHexGrid() {
	let coords = Object.keys(hexMap);
	for (let i = coords.length - 1; i >= 0; --i) {
		let cell = hexMap[coords[i]];
		let hexCoords = cell.getPos();
		let rc = renderingCoords(hexCoords);
		let x = rc[0] * screenInfo.hexScale - screenInfo.x;
		let y = rc[1] * screenInfo.hexScale - screenInfo.y;
		hex(x, y, screenInfo.hexScale, "green", "grey", 2);
		let circuit = cell.getCircuit();
		if (circuit) {
			circle(x, y, screenInfo.hexScale / 5, "gold");
			let nodes = circuit.getNodes();
			for (let ii = nodes.length - 1; ii >= 0; ii--) {
				let node = nodes[ii];
				for (let iii = node.getConnections().length - 1; iii >= 0; iii--) {
					let side = node.getConnections()[iii];
					let ec = edgeCoords(side);
					let color = stateColors[node.getNetwork().getState()];
					line(x, y, x + ec[0] * screenInfo.hexScale, y + ec[1] * screenInfo.hexScale, color, 2 );
				}
			}
		}
	}
}

function draw()
{
	if (screenInfo.dirty) {
		clearCanvas();
		drawHexGrid();
	}
	screenInfo.dirty = false;
}

/*function cap(value, cap)
{
	if (value > cap) return cap;
	if (value < -cap) return -cap;
	return value;
}*/

timer = 0;

function second()
{
	++timer;
}


function tickComponents() {
	for (var i = componentList.length - 1; i >= 0; i--) {
		let comp = componentList[i];
		comp.clock();
	}
	for (var i = networkList.length - 1; i >= 0; i--) {
		let network = networkList[i];
		network.updateState();
	}
	screenInfo.dirty = true;
}

function tick()
{
	tickComponents();
}

function initGame() {
	hexMap = {};
	componentList = [];
	networkList = [];
	const radius = 3;
	for (let r = 0; r < radius; ++r) {
		let pos = hexCoords(0, r, -r);
		for (let dir = 0; dir < (r == 0 ? 1 : 6); ++dir) {
			let delta = sideToDelta(dir + 2);
			for (let n = 0; n < Math.max(1, r); ++n) {
				hexMap[hexCoordsToString(pos)] = new Cell(pos);
				pos = addHexCoords(pos, delta);
			}
		}
	}
	createCircuit(componentList, hexMap[hexCoordsToString(hexCoords(0, 0, 0))], "source");
	screenInfo.dirty = true;
}

window.onload = function() {
	initGame();
	window.setInterval(tick, 1000/20);
	window.setInterval(draw, 1000/60);
	window.setInterval(second, 1000);
}

function click(pos)
{
}

function getMousePos(evt)
{
	let bb = canvas.getBoundingClientRect();
	return [evt.clientX - bb.left - centre[0], 
		evt.clientY - bb.top - centre[1]];
}

function mouseUp(evt)
{
	let pos = getMousePos(evt);
	click(pos);
}

function keyDown(evt)
{
	let e = evt.which || evt.keyCode;
	//console.log(e);
	if (e >= 49 && e <= 53) {
		let i = e - 49;
	}
}
